
#!/bin/bash

# Check if the current OS is macOS
if [[ "$OSTYPE" == "darwin"* ]]; then
    # Get the current user's home directory
    home_dir=$(eval echo ~$USER)
    profiles_dir="$home_dir/Library/Thunderbird/Profiles"

    # Search all folders in the Thunderbird Profiles directory
    find "$profiles_dir" -type f | while read -r file; do
        # Search for "Jira" in the file
        if rg -lq "Jira" "$file"; then
            # Extract the email address and append it to the output file
            rg -o '[A-Za-z0-9._%+-]\+@[A-Za-z0-9.-]\+\.[A-Za-z]\{2,6\}' "$file" >> output.txt
        fi
    done

    # Check if output.txt exists
    if [ -f output.txt ]; then
        # Split the email addresses into local parts and domains, sort by the domain, remove duplicates, and then reassemble the email addresses
       # awk '{print $1 "@" $2}' | sort -u | awk '{print $1 "@" $2}' >> sorted_output.txt # | sort -u | awk '{print $2 "@" $1}' > sorted_output.txt
        awk -F@ '{print $2, $1}'
    else
        echo "No email addresses were found."
    fi
else
    echo "This script only runs on macOS."
fi

